#pragma once
#include "Presentation.h"
#include <iostream>

	namespace Presentations
	{
		class Presentation;

		class ConsoleDraw :
			virtual public Presentation
		{
		public:
			ConsoleDraw();
			~ConsoleDraw();
		};
	}


