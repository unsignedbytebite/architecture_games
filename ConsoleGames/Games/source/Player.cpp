#include "Player.h"

namespace Games
{
	Player::Player(const str name)
	{
		mName = name;
	}

	Player::~Player()
	{
		// Free all held objects
		mObjects.clear();

		// TODO: memory leak?
		//for (us i = 0; i < mObjects.size(); i++)
		//{
		//	if (mObjects.at(i) != NULL)
		//	{
		//		delete mObjects.at(i);
		//		mObjects.at(i) = NULL;
		//	}
		//}
	}

	const str Player::getName()
	{
		return mName;
	}

	void Player::setName(const str name)
	{
		mName = name;
	}

	void Player::assignObject(GameObject * objectToHold)
	{
		mObjects.push_back(objectToHold);
	}

	vector<GameObject*>* Player::getObjects()
	{
		return &mObjects;
	}
	Game* Player::getGame() const
	{
		return mGame;
	}
	void Player::setGame(Game* val)
	{
		mGame = val;
	}
}