#pragma once
#include "Games.h"
#include "GameObject.h"
#include "Game.h"

namespace Games
{
	class Game;
	class GameObject;

	class Player
	{
	public:
		Player(const str name);
		~Player();

		// Get/Set the name of the player
		const str getName();
		void setName(const str name);

		// Assign a object to player to hold
		void assignObject(GameObject* objectToHold);

		// Get owned objects by the player
		vector<GameObject*>* getObjects();

		// Get/Set Game
		Game* getGame() const;
		void setGame(Game* val);

		// Start the player
		virtual void ini() = 0;

		// Update the player
		virtual void update() = 0;
	protected:
		// Player's name
		str mName;

		// Owned game objects by the player
		vector<GameObject*> mObjects;

		// Game the player is in
		Game* mGame;
	};
}
