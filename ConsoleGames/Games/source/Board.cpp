#include "Board.h"

namespace Games
{
	Board::Board(us width, us height)
	{
		mWidth = width;
		mHeight = height;

		mCells = new GameObject*[mWidth * mHeight];

		for (us i = 0; i < mWidth * mHeight; i++)
		{
			mCells[i] = NULL;
		}
	}

	Board::~Board()
	{
		delete[] mCells;
	}

	void Board::set(GameObject* space)
	{
		for (us x = 0; x < mWidth; x++)
		{
			for (us y = 0; y < mHeight; y++)
			{
				set(space, x, y);
			}
		}
	}

	void Board::set(GameObject* space, us x, us y)
	{
		mCells[mWidth * y + x] = space;
	}

	GameObject* Board::getCell(us x, us y)
	{
		// If not in bounds
		if (!(0 <= x && x < mWidth && 0 <= y && y < mHeight))
		{
			return NULL;
		}
		return mCells[mWidth * y + x];
	}

	const str Board::getString()
	{
		str returnString = "";
		returnString += "width: " + to_string(mWidth) + " Height: " + to_string(mHeight) + "\n";

		GameObject* object = NULL;
		for (us x = 0; x < mWidth; x++)
		{
			for (us y = 0; y < mHeight; y++)
			{
				object = getCell(x, y);

				if (object != NULL)
				{
					returnString += "[" + to_string(x) + ", " + to_string(y) + "]" + object->getName() + "\n";
				}
			}
		}

		return returnString;
	}
}