#pragma once
#include "Games.h"
#include "GameObject.h"

namespace Games
{
	class Board
	{
	public:
		Board(us width, us height);
		~Board();

		// Fill the whole board with this object
		void set(GameObject* space);

		// Assign a cell an object with this object
		void set(GameObject* space, us x, us y);

		// Return a cell at the position
		GameObject* getCell(us x, us y);

		// Get a string that represents the state of the board
		const str getString();
	protected:
		// Board grid cells
		GameObject** mCells;

		// Board dimensions
		us mWidth, mHeight;
	};
}

