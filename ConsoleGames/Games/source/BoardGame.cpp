#include "BoardGame.h"

namespace Games
{
	BoardGame::BoardGame(const us boardWidth, const us boardHeight)
	{
		mBoard = new Board{ boardWidth, boardHeight };
	}

	BoardGame::~BoardGame()
	{}

	Board * BoardGame::getBoard() const
	{
		return mBoard;
	}

	void BoardGame::setBoard(Board* val)
	{
		mBoard = val;
	}

	bool BoardGame::ini()
	{
		for (us i = 0; i < mPlayers.size(); i++)
		{
			mPlayers[i]->setGame(this);
			mPlayers[i]->ini();
		}
		return true;
	}

	bool BoardGame::update()
	{
		for (us i = 0; i < mPlayers.size(); i++)
		{
			mPlayers[i]->update();
		}
		return true;
	}

	void BoardGame::placeObject(us x, us y, GameObject* mObject)
	{
		mBoard->set(mObject, x, y);
	}
}