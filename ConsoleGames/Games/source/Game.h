#pragma once
#include "Games.h"
#include "Player.h"

namespace Games
{
	class Player;

	class Game
	{
	public:
		Game()
		{};
		Game(const str name, vector<Player*> players);
		~Game();

		// True if successfully initialised
		virtual bool ini() = 0;

		// Returns the winner of the game
		virtual Player* execute();

		// True if updating
		virtual bool update() = 0;

		// True if freed
		virtual bool free();

		virtual Player* getWinner();

		// Get/Set the name of the game
		const str getName();
		void setName(const str name);

		// Get/Set the name of the game
		const bool isPlaying();
		void setPlaying(const bool flag);

	protected:
		// Game name
		str mName;

		// True if the game is currently looping
		bool mIsPlaying;

		// Players attached to the game
		vector<Player*> mPlayers;
	};
}