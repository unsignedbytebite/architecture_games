#pragma once
#include "Game.h"
#include "Player.h"
#include "Board.h"
#include <vector>

namespace Games
{
	using namespace std;

	class BoardGame :
		virtual public Game
	{
	public:
		BoardGame()
		{};
		BoardGame(const us boardWidth, const us boardHeight);
		~BoardGame();

		// Get/Set Board
		Board* getBoard() const;
		void setBoard(Board* val);

		virtual bool ini() override;

		virtual bool update() override;		

		// Place an object on the board
		void placeObject(us x, us y, GameObject* mObject);
	protected:
		// Board dimensions
		Board* mBoard;
	};
}