#include "GameObject.h"

namespace Games
{
	GameObject::GameObject(const str name, Player* owner)
	{
		mName = name;
	}

	GameObject::~GameObject()
	{}

	void GameObject::setName(const str name)
	{
		mName = name;
	}

	const str GameObject::getName()
	{
		return mName;
	}
	Player* GameObject::getOwner() const
	{
		return mOwner;
	}
	void GameObject::setOwner(Player* val)
	{
		mOwner = val;
	}
}