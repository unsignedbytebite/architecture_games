#include <iostream>
#include "ConsoleReversi.h"
#include "ReversiHuman.h"
#include "ReversiAI.h"

#define HAS_CINEMATICS false

#if HAS_CINEMATICS
#include <windows.h>
#endif

using namespace std;
using namespace Games;
using namespace ConsoleGames;

#if HAS_CINEMATICS
void print(const string& text)
{
	cout << ">>> ";

	for (size_t i = 0; i < text.size(); i++)
	{
		cout << (char)toupper(text[i]);
		Sleep(20 + (rand() % 100));
		//cout << '\a';
	}

	cout << endl;
}

void intro()
{
	print("Shall we play a game?");

	{
		bool choiceOk = false;

		while (!choiceOk)
		{
			str input;
			getline(cin, input);

			choiceOk = (input == "list games" ||
									input == "yes" ||
									input == "ok");
		}
	}

	print("tick tack toe");
	print("reversi");
	print("chess");
	print("poker");
	print("global thermonuclear war");

	{
		bool choiceOk = false;

		while (!choiceOk)
		{
			str input;
			getline(cin, input);

			choiceOk = (input == "reversi");

			if (!choiceOk)
			{
				print("How about another game?");
			}
		}
	}
}
#endif

int main()
{
	#if HAS_CINEMATICS
	while (true)
	{
		intro();
		#endif

		ReversiHuman player1{ "Cj", 0 };
		ReversiAI player2{ "Falken", 1 };

		ConsoleReversi game = ConsoleReversi{ &player1, &player2 };
		cout << "Winner: " << game.execute()->getName() << endl;

		#if HAS_CINEMATICS
		print("How about a nice game of chess?");
	}
	#endif

	return 0;
}